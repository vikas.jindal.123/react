
import CoreConcept from "./components/CoreConcept.jsx";
import Examples from "./components/Examples.jsx";
import Header from "./components/Header/Header.jsx";
import { CORE_CONCEPTS } from "./data.js";
import { useState } from "react";

function App() {

  return (
    <>
      <Header />
      <main>
        <section id="core-concepts">
          <h2>Core Concept</h2>
          <ul>
            {CORE_CONCEPTS.map((conceptItem) => <CoreConcept key={conceptItem.title} {...conceptItem} />)}

          </ul>
        </section>
        <Examples />
      </main>
    </>
  );
}

export default App;
